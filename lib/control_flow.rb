# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete("/a-z/")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  return str[mid] if str.length.odd?
  str[mid - 1..mid] if str.length.even?
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count('AEIOUaeiou')
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (1..num).each { |n| product *= n }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""

  arr.each_index do |idx|
    str << arr[idx]
    str << separator if idx != arr.length - 1
  end

  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  chars = str.chars
  chars.each_with_index do |ch, idx|
    ch.downcase! if idx.even?
    ch.upcase! if idx.odd?
  end

  chars.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  words.each do |word|
    word.reverse! if word.length > 4
  end

  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  nums = (1..n).to_a

  nums.each_with_index do |num, idx|
    if num % 15 == 0
      nums[idx] = "fizzbuzz"
    elsif num % 5 == 0
      nums[idx] = "buzz"
    elsif num % 3 == 0
      nums[idx] = "fizz"
    end
  end

  nums
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  arr.each { |el| new_arr.unshift(el) }
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  return true if num == 2
  (2...num).each { |divisor| return false if num % divisor == 0 }

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_arr = []
  (1..num).each { |factor| factor_arr << factor if num % factor == 0 }
  factor_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factor_arr = []

  (2..num).each do |factor|
    prime_factor_arr << factor if num % factor == 0 && prime?(factor)
  end

  prime_factor_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factor_arr = prime_factors(num)
  prime_factor_arr.count
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens, odds = [], []

  arr.each do |num|
    evens << num if num.even?
    odds << num if num.odd?
  end

  if evens.length == 1
    return evens[0]
  else
    return odds[0]
  end
end
